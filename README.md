# README #

## What is this repository for? ##

* This repo contains a basic fully functioning MEAN project

## Set Up ##
These instructions assume that you have git, node, and bower installed globally on your machine.

1. Install MongoDB and run a local instance of it on your machine:
    * [Instructions for Linux](https://docs.mongodb.org/manual/administration/install-on-linux/)
    * [Instructions for Windows](https://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/)
    * [Instructions for OS X](https://docs.mongodb.org/manual/tutorial/install-mongodb-on-os-x/)

2. Clone this repo
    * In your terminal, execute the command: `git clone https://nusgloria@bitbucket.org/iss_startup/cms.git`

3. Install node dependencies
    * Change directory (cd) to /cms and execute the command: `node install`

4. Install bower_components (includes angular, bootstrap, jQuery etc.)
    * cd to /cms/server/public and execute the command: `bower install`

5. Run the server
    * cd to /cms/server and execute the command: `node app.js`
    * You should see the following message in your terminal if the server is running properly:
    'Express started on http://localhost:8000;  press Ctrl-C to terminate.'

6. Open up a browser (except IE ;) ), type 'http://localhost:8000/' in the address bar, hit enter and enjoy!

_Hint: there is lots of logging data displayed in the console that will give you an idea of how the code is executed so open up the console and take a look._

##Purpose##

The purpose of this project is to.

* Provide you with an example of what you should be able to do by the end of the 40 days
* Give you ideas of how to implement some common functionality like
    * All CRUD functions (Get, and post requests)
    * User authentication using sessions
    * File uploads, and more.
* Provide some inspiration for functionality and maybe style


##How it works##

###Overview###

Brief descriptions of how each component works are included in the code as comments so here I will simply be covering what each component does.

###Data Storage###
In the CMS, 3 types of data is stored, the users' data, the page data, and the image file data. There are 2 MongoDB collections, one for user data and one for the page data. Each page's information is stored as a separate document in this collection. The attributes of these documents can be found in _cms/server/models/x.js_ where x is users or pages based on the data you are interested in. In these files your will find examples of how to compile a model with Mongoose. A model is simply a constructor generated from the schema you define. The model is used to create, update, query and remove documents. Each instance of the model is a document. [Read more about Mongoose models here.](http://mongoosejs.com/docs/models.html) Also note, upon your initial of _app.js_, some initial data will be seeded by the _data.js_ file. Delete this file if the app is deployed with a non-local DB server.

###CRUD###

####Backend####
All of the data can be 'read' by anyone on the web via an API call to our server. All of the API get routes are found in _cms/server/routes/routes.js_. This file uses `express.Router()` to handle the routes. Here you will see examples of how to query a Mongo database using Mongoose models. __[Explore the syntax of querying using models in Monoose.](http://mongoosejs.com/docs/queries.html)__ From the code handling each api get request, it should be evident that a json array is returned in response using `res.json`. This array consists of documents from the database the match the query; each documents is represented as an object. Which attributes of the object to return in the response can be specified in the query. `Array.map()` is used in the query callback to format the data before it is sent to the client. [Read more about Array.map() here.](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map) You can see in the app.js file how CORS is used to allow access to the api routes from external domains. This way, everyone can access the JSON data for pages and users.

All of the 'post' routes can be found in _cms/server/app.js_ from lines 76 to 376. These handle all of the post requests from the client which are used to create new documents, update documents and delete them. _In the future, the delete and update functions should implement special authentications checks to ensure that the request come from a client with the correct authorization. This can be done using the sessionKey stored in the Mongo session store._

The Mongoose syntax used can be found here:

* [Creating](http://mongoosejs.com/docs/models.html)
* [Reading](http://mongoosejs.com/docs/queries.html)
* [Updating](http://mongoosejs.com/docs/api.html#model_Model.update)
* [Deleting](http://mongoosejs.com/docs/models.html)

In the CMS, the create and update methods are merged using the upsert option: `{upsert : true}`. Upsert means update/insert. Meaning, Mongoose will update the object if the query finds it and create a new object with the given data if the query fails to find it.

####Frontend####

In the frontend, Angular services are used to send get and post requests to the server to implement CRUD functionality. See _cms/server/public/index.js_ for the services code; it is under the "Services" title. We will be looking at the API Get, Delete and Post services. These services can be used as an example of how to set up services using `app.factory`.

The `gettingFactory.getter(url, isNotAPIReq)` is simple; it takes 2 arguments, url and isNotAPIReq. `$q` from __[Angular's promise API](https://docs.angularjs.org/api/ng/service/$q)__ is used to create a promise called deferred. This promise is __resolved__ when the `$http.get` promise is resolve and deferred is __rejected__ if the http get request fails. In the end, the deferred promise is returned. This structure allows us to externalize the `$http.get()` promise and expose it to controllers that use the `gettingFactory.getter` service because it now returns a promise. In essence, the caller can user the `promise.then(successCallback, failureCallback)` on the service because it returns a promise. This can be seen in all controllers that use the getting service but an example is `PageCtrl`. Many other services use a similar structure so it is important to clearly understand promises and how to use them. By contrast, `deletingFactory.deleter(mail, handler)` does not return a promise thus the success and failure callbacks are executed in the service code and cannot be exposed to the controller calling them. A practical implication of this is that we can specify what to do upon a successful `gettingFactory.getter` call, for instance, we can load the page data. However, for `deletingFactory.deleter` we cannot specify what to do or even what block of code to execute upon successfully deleting something. Instead, the service defines the success action and in this case it reload the page upon a successful deletion.

The delete and post factories simply send some data collected from the controllers in the variable `mail`. A primary key in this `mail` is used by the server routes to identify the particular document and modify it. These factories do not return any data to the client. Meanwhile, the gettingFactory, sends a request to an api endpoint that sends an array of json data. This data is stored by the controller and displayed. There are lots of other arguments in the post request which tell it behave slightly differently based on the parameters specified by the basic concept is the same.

###File Uploads###

The file upload functionality is implemented using [Mongo's GridFS System](https://docs.mongodb.org/manual/core/gridfs/) that stores large files as chunks. It makes two collections, one with all of the chunks and another will all of the file info. The latter stores the metadata telling GridFS about which chunks of data belong to which file.

Benefits of this:

* Allows you to store pretty large files like audio, videos and images
* Allows you to load parts of the file. Ex: you can load just the start of a video as a preview

Drawbacks:

* Slower than simply storing uploaded files in a directory

For this application, we use very simply GridFS functionality, it is able to do a lot more complex things that are beyond the scope of this project

####Backend####
The code handling file uploads can be found at _cms/server/app.js_ lines 275 down under _//File upload controller_. First, the express middleware for connecting to GridFS is configured, this middleware is called __[gridfs-stream](https://github.com/aheckmann/gridfs-stream)__. Then, several functions are created that deal with the files in GridFS, these functions are all attributes of the object called __uploads__. [Find more info on how the syntax of gridfs-stream works here.](https://github.com/aheckmann/gridfs-stream)

* `upload.create`: this function creates a new file in GridFS. It creates a writeStream in GridFS and writes the request (req) data to the DB. The req comes from the client and will be explained in the frontend section.
* `upload.read`: this function queries GridFS for all files that match the filename specified in the request parameters. If the files exists it creates a read stream. Then creates a writeHead in the response. Upon receiving data `readStream.on('data', //callback)` a callback function writes the data in the response. Once the writing is ended, the response is closed and the file should be loaded in the browser.
* `upload.delete`: this function simply queries the file by filename and deletes all filenames that match
* `upload.deleteFromUrl`: this function allows you to delete a file by simply navigating to a url. The route using this function is disabled to prevent accidental file deletions

The routes that handle requests regarding file uploads are found below these function definitions.

* `app.get('/api/upload/:filename', upload.read)`: this route handler accepts the filename parameter from the client and calls the _upload.read_ function to send the file that matches the filename.
* `app.post('/api/upload/', upload.create)`: accepts file and calls upload.create to save the file in GridFS
* `app.post('/api/img/del/', upload.delete)`: accepts filename data and calles upload.delete to delete the file that matches the filename

_Note: the filenames are used as a primary key so they have to be unique. Else, you could delete all files that have the same filename. To ensure the filenames are unique, all files are renamed as a random string in the frontend._

####Frontend###

> #####Using External Angular Modules: #####
> In the frontend, we are using an external angular module called __[ngFileUpload.](https://github.com/danialfarid/ng-file-upload)__ You can see how this and any other external angular module can be injected in the first line: `var app = angular.module("MyCMSApp", ["ui.router", "ngFileUpload"]);` You can see that our app has 2 dependencies or 2 external modules it uses: ui.router and ngFileUpload. If you use any external modules, you should install them in bower_components using `bower install someones_modules --save` and then inject it just as ngFileUpload is injected.

ngFileUpload is a very useful and simple tool that allows us to create a file upload form in angular and modify it using built-in angular directives. Let's look at an example from _cms/server/public/views/pageEditor.html_. Scrolling to line 46 around `<p>Drop your file here or..</p>` there are many ngf-x attributes inserted in the divs. The attributes allow us to control:

* Where the file must be dropped to be uploaded: `ngf-drop`
* How large the file can be: `ngf-max-sixe="100MB"`
* And more, see the [ngf documentation here](https://github.com/danialfarid/ng-file-upload) for a thorough description of the different directives and what they do.

ngFileUpload also allows you to show thumbnails of the file to be uploaded using `ngf-thumbnail`. You can also force the user to resize images or crop them before upload etc. See the documentation for all these cool things! The most important part of ngFileUpload is that it allows us to save the file that is uploaded as an ng-model. In our case this is `pageEdit.file`. This variable's value changes every time a new file is selected to be uploaded. When the "Add selected file" button is pressed, this file is uploaded to the server where our gridfs routes handle it. So what happens when this button is pressed?

Let's look at _index.js/pageEditorCtrl//File submitting func_. This function uses `Upload.upload` which is an ngFileUpload service that allows you to send a file stored in an ng-model to the server. We specify the url that we want to upload the file to in this service. We also rename the files as a 6 character random string generated by `idFactory.generate(6)`. A post request is sent to _/api/upload/_ and the route handler saves this file using `upload.create` from the backend section

This service returns a promise once this promise is resolved, we add its name and the style for its thumbnail (the thumbnail in imageOptionHolder div in _pageEditor.html_) to an array. The file names in this array are added to the page.imageFilenames attribute of the server. This array holds all of the image options for a particular page. The Singapore page for instance, can have 3 filenames which correspond to 3 image options for the Singapore page. If a new image is uploaded for Singapore, then Singapore's page.imageFilenames will have 4 entries with the forth being the new image that is added.

Once an image is selected from the imgOptionHolder div (by clicking it), its style is changed to indicate that it is chosen, and its filename is set as the value of the `headerImg: ` attribute. These images are received simply by sending a get request to _/api/upload/:filename_. We saw earlier how the server handles such a request with `upload.create`. We can see this in action in the _pageEditor.html_ file where `ng-src="/api/upload/{{img.Id}}"` tells the img tag to get an image from _/api/upload/:filename_ based on the imd.Id angular model.