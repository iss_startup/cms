var express = require('express');
var handlebars = require('express-handlebars');
var cors = require('cors');
var app = express();

//Data stored in the server during a session.
var serverSessionData = {};

app.engine('handlebars', handlebars({defaultLayout: 'mainlayout'}));
app.set('view engine', 'handlebars');

//Connecting to local DB
var mongoose = require('mongoose');
var opts = {
    server : {
        socketOption: {keepAlive: 1}
    }
};
var DBURL = 'mongodb://localhost/cms_db';
switch(app.get('env')){
    case 'development':
        mongoose.connect(DBURL, opts);
        break;
    case 'production':
        mongoose.connect(DBURL, opts);
        break;
    default:
        throw new Error('Unknown execution environment:' + app.get('env'));
}

//Import routes
var routes = require('./routes/routes.js');

//Schemas for Mongo
var Page = require('./models/pages.js');
var User = require('./models/users.js');
var Update = require('./models/updates.js');

//Mongo DB commands
//Seed initial data
require('./models/data.js');

//Importing cookieSecret to sign cookies
var credentials = require('./credentials.js');

/*
============================================
Middleware
============================================
 */

//Body parser for parsing request
app.use(require('body-parser').urlencoded({extended: true}));

//Using mongo for session storage
var MongoSessionStore = require('session-mongoose')(require('connect'));
var sessionStore = new MongoSessionStore({
    url: DBURL
});


//Cookie parser for parsing cookies
app.use(require('cookie-parser')(credentials.cookieSecret));

//Session parser for sessions
app.use(require('express-session')({
    resave: false,
    saveUninitialized: false,
    secret: credentials.cookieSecret,
    store: sessionStore
}));

//Body parser for file uploads
var busboyBodyParser = require('busboy-body-parser');
app.use(busboyBodyParser());
/*
============================================
 Routes
============================================
 */


app.get("/", function(req,res){
    //Upon loading the index, all session data is wiped
    "use strict";
    res.render("index");
});

app.get('/identity', function(req, res){
    "use strict";
    console.log("app.js :86: ", req.session.sessionKey);
    if (serverSessionData[req.session.sessionKey]){
        //return data for this sessionKey to client
        var requestedUsername = serverSessionData[req.session.sessionKey];
        console.log("The username is: ", requestedUsername);
        User.find({username: requestedUsername},
            function(err, user){
                if(err) return res.status(500).send('Error: ' + err);
                var data =
                    {
                        activeUser : {
                            username:user[0].username,
                            name: user[0].name,
                            email: user[0].email,
                            permission: user[0].permission,
                        }
                    };
                console.log(data);
                res.json(data)
            });
    }
    else {
        res.send(401, {error: "User not logged in."});
    }
});

app.post("/login", function(req, res){
    "use strict";
    console.log(req.body.loginCredentials);
    User.find(function(err, users){
        var isAuthorized = false;
        if(err) return res.status(500).send('Error: ' + err);
        //Iterates through existing user collection on Mongo
        //and checks if the provided username and pwd match any documents in the collection
        for (var i = 0; i < users.length; i++ ) {
            if (req.body.loginCredentials.username == users[i].username && req.body.loginCredentials.password == users[i].password) {
                isAuthorized = true;
                break;
            }
            else {
                isAuthorized = false;
            }
        }
        //If user is authorized, create a random key and assigns the username to this key
        //This key is a property of the serverSessionData object and can be accessed globally
        //Also sends the same sessionKey to the client in the client's session
        if(isAuthorized){
            var sessionKey = Math.random().toString();
            serverSessionData[sessionKey] = req.body.loginCredentials.username;
            req.session.sessionKey = sessionKey;
            console.log(serverSessionData);
            res.sendStatus(200);
        }
        else{
            //send error
            res.status(401).send({error: "Invalid credentials"});
        }
        //when a get request is made for the user's data (data used by angular module to control access)
        //the get request handler will check to see if the sessionKey in the requests's session object
        //is a key in the server's serverSessionData object.
        //If it is a key, then the handler sends the corresponding username's data
        //Else it sends an error
        //This makes sure no one is fabricating session data to get access

    });
});

app.post("/logout", function(req, res){
    "use strict";
    req.session.sessionKey = undefined;
    delete serverSessionData[req.session.sessionKey];
    res.sendStatus(200);
    //console.log(sessionStore.sessionKey);
});

//Using CORS for all API calls
app.use('/api', cors());

var router = express.Router();

//routes will be prefixed with api/
app.use('/api', routes);

app.post('/api/users/del', cors(), function(req, res){
    console.log('Delete req for username: ' + req.body.usernameToDelete);
    User.remove({username: req.body.usernameToDelete},
        function(err){
            if(err){
                console.log(err.stack);
                return res.sendStatus(201);
            }
            console.log('Successfully removed above!!!');
            return res.sendStatus(201);
        }
    );
});


app.post('api/updates', function(req, res){
    Update.update(
        {$set: 
            {
                author: req.body.author,
                description: req.body.description,
                updatedAt: req.body.updatedAt
            }
     },
  
    {upsert: true},
    function(err){
        if(err){
            console.log(err.stack);
            return res.sendStatus(500);
        }
       console.log('Successfully userted above!!!');
        return res.sendStatus(202);
    }
    )
});

app.post('/api/pages/del', cors(), function(req, res){
    console.log('Delete req for page: ' + req.body.idNum);
    Page.remove({idNum: req.body.idNum},
        function(err){
            if(err){
                console.log(err.stack);
                return res.sendStatus(500);
            }
            console.log('Successfully removed above!!!');
            return res.sendStatus(202);
        }
    );
});


app.post('/api/pages', function(req, res){
    console.log(req.body.newPage.imgFilenames);
    console.log(req.body.newPage);
    Page.update(
        {idNum: req.body.newPage.idNum},
        {$set : {
            name: req.body.newPage.name,
            author: req.body.newPage.author,
            "last-modified": req.body.newPage["last-modified"],
            "header-img": req.body.newPage["header-img"],
            imgFilenames: req.body.newPage.imgFilenames,
            caption: req.body.newPage.caption,
        }},
        {upsert: true},
        function(err){
            if(err){
                console.error(err.stack);
                return res.sendStatus(500);
            }
            console.log('Successfully upserted above!!!');
            return res.sendStatus(202);
        }
    );
});

app.post('/api/users', function(req, res){
    console.log(req.body.newUser);
    User.update(
        {username: req.body.newUser.username},
        {$set : {
            name: req.body.newUser.name,
            email: req.body.newUser.email,
            permission: req.body.newUser.permission,
            password: req.body.newUser.password
        }},
        {upsert: true},
        function(err){
            if(err){
                console.error(err.stack);
                return res.sendStatus(500);
            }
            console.log('Successfully upserted above!!!');
            return res.sendStatus(202);
        }
    );
});



//File upload controller
var _ = require('lodash');
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
var gfs = new Grid(mongoose.connection.db);


var upload = {};
upload.create = function(req, res){
    "use strict";
    var part = req.files.headerImage;
        var writeStream = gfs.createWriteStream({
            filename : part.name,
            mode : 'w',
            content_type : part.mimetype
        });

        writeStream.on('close', function(){
            return res.status(200).send({
                message : 'Success'
            });
        });

        writeStream.write(part.data);
        writeStream.end();
};

upload.read = function(req, res){
    "use strict";
    console.log(req.params.filename);
    gfs.files.find({filename: req.params.filename}).toArray(function(err, files){
        if(files.length===0){
            return res.status(400).send({
                message : 'File not found'
            });
        }

        res.writeHead(200, {'Content-Type' : files[0].contentType});

        var readStream = gfs.createReadStream({
            filename : files[0].filename
        });

        readStream.on('data', function(data){
            res.write(data);
        });

        readStream.on('end', function(){
            res.end();
        });

        readStream.on('error', function(err){
            console.log('An error occurred!', err);
            throw err;
        });
    });
};

upload.delete = function(req, res){
    "use strict";
    console.log(req.body);
    gfs.remove({filename: req.body.fileToDelete}, function(err){
        if(err) {
            console.log("Couldn't delete file: ", err);
            res.sendStatus(400);
            return
        }
        console.log("Removed file successfully");
        res.sendStatus(200);
    })
};




upload.deleteFromURL = function(req, res){
    "use strict";

    gfs.remove({filename: req.params.file}, function(err){
        if(err) {
            console.log("Couldn't delete file: ", err);
            res.sendStatus(400);
            return
        }
        console.log("Removed file successfully");
        res.sendStatus(200);
    })
};



//Routes for file requests
app.get('/api/uploadExample', function(req, res){
    "use strict";
    res.render('uploadExample');
});
app.post('/api/img/del', upload.delete);
app.get('/api/upload/:filename', upload.read);
app.post('/api/upload', upload.create);


//app.get('/upload/delNow/:file', upload.deleteFromURL);

app.use(express.static(__dirname + "/public"));

app.set('port', process.env.APP_PORT || 8000);

app.listen(app.get('port'), function(){
    console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.');
});