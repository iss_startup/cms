var mongoose = require('mongoose');

var updateSchema = mongoose.Schema({
	author: String,
	description: String,
	updatedAt: String
});

var Updates = mongoose.model('Updates', updateSchema);
module.exports = Updates;
