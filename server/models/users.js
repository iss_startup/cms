var mongoose = require('mongoose');

var usersSchema = mongoose.Schema({
	username: String,
	name: String,
	email: String,
	permission: String,
	password: String
});

var User = mongoose.model('User', usersSchema);
module.exports = User;