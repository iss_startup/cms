var app = angular.module("MyCMSApp", ["ui.router", "ngFileUpload"]);

app.config(["$stateProvider", "$urlRouterProvider", MyCMSConfig]);
app.run(["$rootScope", function($rootScope){
    "use strict";
    $rootScope.normalUrl = 'http://localhost:8000/';
    $rootScope.apiUrl = 'http://localhost:8000/api/';
    $rootScope.activeUser = {};
}]);
app.controller("MyCMSCtrl", [MyCMSCtrl]);

/*
====================================
Services
====================================
 */
//Unique ID Generator
app.factory('idFactory',[function(){
    "use strict";
    var factory = {};
    factory.generate = function(length){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < length; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };
    return factory;
}]);


//API Get service
app.factory('gettingFactory', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){
    var factory = {};
    factory.getter = function(url, isNotAPIreq){
        var deferred = $q.defer();
        var newUrl;
        if (isNotAPIreq){
            newUrl = $rootScope.normalUrl + url;
        }
        else{
            newUrl = $rootScope.apiUrl + url;
        }
        //console.log(newUrl);
        $http.get(newUrl).then(function(response){
                deferred.resolve(response);
            },
            function(response){
                deferred.reject(response);
            }
        );
        return deferred.promise;
    };

    factory.test = function(){
        console.log('connected to service');
    };

    return factory;
}]);

//API Delete Service
app.factory('deletingFactory', ['$http', '$state', '$rootScope',function($http, $state, $rootScope){
    var factory = {};
    factory.deleter = function(mail, handler){
        console.log($rootScope.apiUrl + handler + '/del')
        $http({
            method: 'POST',
            url: $rootScope.apiUrl + handler + '/del',
            data: $.param(mail),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(){
                console.log('Post Successful, deleted');
                $state.reload();

            },
            function(){
                console.log('Error: Post Failed, not deleted');
            }
        )
    };
    factory.tester = function(){
        console.log('connected to deletingFactory');
    };
    return factory;
}]);

//API Post Service
app.factory('postingFactory', ['$http', '$state', '$q', '$rootScope', function($http, $state, $q, $rootScope){
    var factory = {};
    factory.poster = function(mail, url, redirectTo, dontResolve, isNotAPIreq){
        function httpPromise() {
            "use strict";
            var postUrl;
            if (isNotAPIreq){
                postUrl = $rootScope.normalUrl + url;
            }
            else{
                postUrl = $rootScope.apiUrl + url;
            }
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: postUrl,
                data: $.param(mail),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function(){
                    "use strict";
                    deferred.resolve();
                }
                ,function(response){
                    "use strict";
                    deferred.reject(response);
                });
            return deferred.promise;
        }

        if (dontResolve){
            return httpPromise();
        }
        else{
            httpPromise().then(function(){
                    console.log('Post Successful');
                    $state.go(redirectTo);
                },
                function(){
                    console.log('Error: Post Failed');
                });
        }

    };

    factory.tester = function(){
        console.log('connected to postingFactory');
    };
    return factory;
}]);

//Blocks the default state change and executes doInstead() instead of loading requested page
app.factory('stateChangeFactory', ['$rootScope', '$state', function($rootScope, $state){
    "use strict";
    var factory = {};
    factory.preventCalledTransition = function(doInstead){
        $rootScope.$on('$startStateChange', function(event, toState){
            event.preventDefault();
            console.log("preventing default");
            doInstead();
        });
    };

    return factory;
}]);

//Service parses $rootScope.activeUser for convenience and to prevent
// accidentally changing values
app.factory('identifyingFactory', ['$q', '$http', '$rootScope','$state', 'gettingFactory', function($q, $http, $rootScope, $state, gettingFactory){
    "use strict";
    var identity = $rootScope.activeUser,
        factory = {};
    factory.test = function(){
        console.log("Is identifying...");
    };

    factory.isUserLoggedInChecker = function(){
        var deferred = $q.defer();
        gettingFactory.getter('identity', true).then(
            function(response){
                $rootScope.activeUser = response.data.activeUser;
                $rootScope.activeUser.loggedIn = true;
                $rootScope.activeUser.allowTo = false;
                console.log("Identifying Factory:: isUserLoggedInChecker:: ",$rootScope.activeUser);
                console.log('Identifying Factory:: isUserLoggedInChecker:: Get success! Received user data.');
                deferred.resolve();
            },
            function(){
                console.log("Get Failed, user not logged in.");
                deferred.reject();
            }
        );
        return deferred.promise;
    };

    factory.isUserActive = function(){
        console.log("Identity is empty: ", Object.keys($rootScope.activeUser).length == 0);
        if (Object.keys($rootScope.activeUser).length == 0) {
            return false;
        }
        else {
            return true;
        }
    };

    factory.activeUsername = function(){
        return $rootScope.activeUser.username;
    };

    factory.activeUserRole = function(){
        return $rootScope.activeUser.permission;
    };

    return factory;
}]);

app.factory('authorizingFactory', ['$q', '$state', '$rootScope', '$location', 'stateChangeFactory', 'identifyingFactory',
    function($q, $state, $rootScope, $location, stateChangeFactory, identifyingFactory){
        "use strict";
        var factory = {};
        factory.test = function(){
            console.log("Is authorizing");
        };
        factory.authorize = function(){
            var self = this;
            identifyingFactory.test();

            //Do role check
            self.role = $rootScope.activeUser.permission;
            self.toStateRoles = $rootScope.toState.data.roles;
            console.log("AuthFac:: user is active and logged in");
            //loop checks if the users role matches with any roles the state allows
            console.log("AuthFac:: user role ", self.role);
            for(var i = 0; i < self.toStateRoles.length; i++){
                console.log("AuthFac:: index ", i);
                if(self.toStateRoles[i] == self.role){
                    $rootScope.activeUser.allowTo = $rootScope.toState.url;
                    break;
                }
            }
            console.log("AuthFac:: allowTo ", $rootScope.activeUser.allowTo);
        };
        return factory;

    }]);
/*
 ====================================
 Directives
 ====================================
 */
app.directive('deleteWarning', function(){
    "use strict";
    var directive = {
        restrict: 'E',
        transclude: true,
        scope: {
            itemToDelete: '=itemToDelete',
            delString : '=delString',
            deleteItem : '&onDelete',
            abort : '&onAbort',
        },
        templateUrl: 'views/partials/deleteWarning.html'
    };
    return directive;
});

/*
 ====================================
 Routes
 ====================================
 */
function MyCMSConfig($stateProvider, $urlRouterProvider) {
    "use strict";

    $stateProvider
        .state("site",{
            url:"/",
            templateUrl:"views/site/site.html",
            data : {
                public : true
            }
        })
        .state("login", {
            url: "/login",
            templateUrl: "views/login.html",
            controller: ["$log", "$state", "$rootScope", "identifyingFactory", "postingFactory", LoginStateCtrl],
            controllerAs: "login",
            data : {
                public : true
            }
        })
        .state("main", {
            url: "/main",
            templateUrl: "views/main.html",
            /*resolve: {
             onEnter : ['authorizingFactory',
             function (authorizingFactory) {
             return authorizingFactory.authorize();
             }]
             },*/
            controller: ["$state", "$rootScope", "postingFactory", MainStateCtrl],
            controllerAs: "mainCtrl",
            data : {
                public : true
            }
        })
        .state("main.accessDenied",{
            url: "/accessDenied",
            templateUrl: "views/accessDenied.html",
            data: {
                public : true
            }
        })
        .state("main.pages", {
            url: "/pages",
            templateUrl: "views/pages.html",
            controller:[ "$state", "gettingFactory", "deletingFactory", PageCtrl ],
            controllerAs: "page",
            data: {
                public: false,
                roles : ['Administrator', 'Editor']
            }
        })
        .state("main.dashboard", {
            url: "/dashboard",
            templateUrl: "views/dashboard.html",
            controller: ["gettingFactory", DashboardCtrl],
            controllerAs: "dashboard",
            data: {
                public  : false,
                roles : ['Administrator', 'Editor']
            }
        })
        .state("main.users", {
            url: "/users",
            templateUrl: "views/users.html",
            controller: [ "$state", "gettingFactory", "deletingFactory", userCtrl ],
            controllerAs: "user",
            data: {
                public  : false,
                roles : ['Administrator']
            }
        })
        .state("main.settings", {
            url: "/settings",
            templateUrl: "views/settings.html",
            data: {
                public  : false,
                roles : ['Administrator', 'Editor']
            }
        })
        .state("main.pageEditor", {
            url: "/pageEditor/:idNum",
            templateUrl: "views/pageEditor.html",
            controller: [ "$state", "$stateParams", "$rootScope", "Upload", "gettingFactory", "postingFactory", "deletingFactory", "idFactory", pageEditorCtrl ],
            controllerAs: "pageEdit",
            data: {
                public  : false,
                roles : ['Administrator', 'Editor']
            }
        })
        .state("main.userEditor", {
            url: "/userEditor/:username",
            templateUrl: "views/userEditor.html",
            controller: [ "$stateParams", "gettingFactory", "postingFactory", userEditorCtrl ],
            controllerAs: "userEdit",
            data: {
                public  : false,
                roles : ['Administrator']
            }
        });

    $urlRouterProvider.otherwise("/");
}

/*
 ====================================
 Listener
 ====================================
 */


//For each attempt to access a state, this function checks if the user is authorized using the
//corresponding factory
app.run(['$rootScope', '$state','authorizingFactory', 'identifyingFactory',
    function($rootScope, $state, authorizingFactory, identifyingFactory) {
    "use strict";

    $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams){
        console.log("AuthListener:: StateChangeStarted to ", toState.url);

        $rootScope.toState = toState;
        //Only authorize if the toState is not public.
        //If user is logged in and has proper authorization, then allow.
        if(toState.data.public || ($rootScope.activeUser.loggedIn && ($rootScope.activeUser.allowTo == toState.url))){
            console.log("AuthListener:: Auth cleared");
        }
        //If logged in but lacks proper authorization, perform authorization check to see if user qualified
        else if ($rootScope.activeUser.loggedIn){
            console.log("AuthListenfer:: Auth started");
            event.preventDefault(); //Stops regular loading of page
            authorizingFactory.authorize();
            //If fails to qualify, go to access denied
            if ($rootScope.activeUser.allowTo != toState.url) {
                console.log("AuthListener:: Need different role");
                $state.go('main.accessDenied');
                return
            }
            //Else go to requested state
            //Going to this state forces this listener to run again but this time
            //The value of allowTo will allow the user to go to desired state at
            //the first if statement
            $state.go(toState, toStateParams);
        }
        else {
            event.preventDefault(); //Stops regular loading of page
            //Sending sever request to see if the user is logged in
            identifyingFactory.isUserLoggedInChecker().then(
                //If success, send user to desired location (This will run the listener again but now with active user)
                function() {
                    console.log("AuthListener:: User is already logged in, authorizing now");
                    authorizingFactory.authorize();
                    $state.go(toState, toStateParams);
                },
                //Else got to login page
                function(){
                    console.log("AuthListener:: Access Denied: User is not logged in, going to login page");
                    $state.go('login');
                }
            );
        }
    });
}]);

/*
 ====================================
 Controllers
 ====================================
 */

function MyCMSCtrl() {
    "use strict";

}

function LoginStateCtrl($log, $state, $rootScope, identifyingFactory, postingFactory) {
    "use strict";
    var vm = this;
    vm.status = "";
    if($)
    vm.loginCheck = function(){
        //console.log($rootScope.activeUser);
        vm.newData= {
            loginCredentials : vm.userDetails
        };
        console.log(vm.newData);
        postingFactory.poster(vm.newData, 'login', "main.dashboard", true, true)
            .then(
                function(){
                    console.log("LoginStateCtrl:: Post success...logging in....");
                    identifyingFactory.isUserLoggedInChecker().then(
                        function(){
                            console.log("LoginStateCtrl:: Login Success");
                            $state.transitionTo("main.dashboard");
                        },
                        function(){
                            console.log("LoginStateCtrl:: isUserLoggedInChecker Failed");
                        }
                    );
                },
                function(){
                    vm.invalidCreds = true;
                    console.log("LoginStateCtrl:: Post Failed: invalid credentials");
                });
    };
}

function DashboardCtrl(gettingFactory){
    "use strict";
    var vm = this;
    gettingFactory.getter('updates')
        .then(function(response){
            vm.updates = response.data;
        }, function(errResonse){
            console.error('Error occurred fetching data.');
        });
}

function MainStateCtrl($state, $rootScope, postingFactory) {
    "use strict";
    console.log($rootScope.activeUser);
    var vm = this;
    vm.logOut = function(){
        $rootScope.activeUser = {};
        $rootScope.isAuthorized = false;
        $rootScope.isAuthenticated = false;
        postingFactory.poster({}, "logout", "", true, true)
            .then(function(){
                    console.log("logout success!");
                    $state.transitionTo("login");
                },
                function(){
                    console.error("Could not logout: Server Error")
                });
    }
}

function PageCtrl($state, gettingFactory, deletingFactory){
    "use strict";

    var vm = this;
    vm.showWarning = false;

    //vm.pages = pages;

    gettingFactory.getter('pages')
        .then(function(response){
            vm.pages = response.data;
        }, function(errResonse){
            console.error('Error occurred fetching data.')
        });

    vm.editPage = function(index){
        console.log(">>edit pages " + index);
        console.log("PageCtrl:: editPage:: ",vm.pages[index].idNum);
        $state.go("main.pageEditor", {idNum: vm.pages[index].idNum});
    };

    vm.preDelete = function(index){
        vm.data = {
            pageToDelete: vm.pages[index].name,
            idNum : vm.pages[index].idNum,

        };
        vm.showWarning = true;
    };

    vm.deletePage = function(){
        if(vm.delString === "DELETE"){
            vm.delString = "";
            vm.showWarning = false;
            deletingFactory.deleter(vm.data, 'pages');
        }
    };

    vm.abort = function(){
        vm.showWarning = false;
        vm.data = {};
        vm.delString = "";
    };

    vm.addPage = function(){
        var tmp = Math.floor(Math.random() * 10000000000);
        console.log(tmp);
        $state.go("main.pageEditor", {idNum: tmp});
    };

}

function pageEditorCtrl($state, $stateParams, $rootScope, Upload, gettingFactory, postingFactory, deletingFactory, idFactory){
    "use strict";
    var vm = this;

    gettingFactory.getter('pages')
        .then(function(response){
            vm.pages = response.data;
            vm.registeredPageIds = vm.pages.map(function(page){
                return page.idNum;
            });
            //Checks if the idNum of the page being edited is registered. If so, autofills that info
            for (var i = 0; i < vm.registeredPageIds.length; i++){

                if ($stateParams.idNum == vm.registeredPageIds[i]){
                    vm.page = vm.pages[i];
                    vm.name = vm.page.name;
                    vm.caption = vm.page.caption;
                    vm.author = $rootScope.activeUser.username;
                    vm.lastmodified = vm.page["last-modified"];
                    vm["header-img"] = vm.page["header-img"];
                    if (vm.page.imgFilenames){
                        vm.imgFilenamesStyle = vm.page.imgFilenames.map(function(img){
                            var obj = {
                                Id : img,
                                Style : ""
                            };
                            if (img == vm["header-img"]){obj.Style = 'border: 5px solid #ffd72f'}
                            return obj;
                        });
                    }
                    console.log(vm.imgFilenamesStyle);

                    break;
                }
            }
            if(!vm.imgFilenamesStyle){
                vm.imgFilenamesStyle = [];
            }
        }, function(errResonse){
            console.error('Error occurred fetching data.')
        });

    vm.submitChange = function(){
        vm.newData = {
            newPage : {
                name: vm.name,
                author: $rootScope.activeUser.username,
                "last-modified": (new Date()).toString().slice(0,21),
                "header-img": vm["header-img"],
                caption: vm.caption,
                imgFilenames: vm.imgFilenamesStyle.map(function(img){
                    return img.Id
                }),
                idNum: $stateParams.idNum
            }
        };
        console.log("pageEditorCtrl:: sumbitChange:: ", vm.newData);
        postingFactory.poster(vm.newData, 'pages', "main.pages");

    };

    //File submitting func
    vm.addFile = function(){
        vm.newFileId = idFactory.generate(6);
        vm.file.upload = Upload.upload({
            url: '/api/upload',
            data : {headerImage : Upload.rename(vm.file, vm.newFileId)}
        });
        vm.file.upload.then(
            function(){
                console.log("pageEditorCtrl:: FileSubmit:: success!");
                vm.imgFilenamesStyle.push(
                    {
                        Id: vm.newFileId,
                        Style : ""
                    }
                );
                console.log("pageEditorCtrl:: FileSubmit:: ", vm.imgFilenamesStyle);
                vm.file = null;
                vm.newFileId = "";
            },
            function(){
                console.log("pageEditorCtrl:: FileSubmit:: failure :( ");
                vm.file = null;
                vm.newFileId = "";
            }
        )
    };
    
    vm.deleteImg = function(img){
        vm.deleteThis = {fileToDelete : img.Id};
        console.log("pageEditorCtrl:: deleteImg:: ", vm.deleteThis);

        console.log(vm.imgFilenamesStyle);
        postingFactory.poster(vm.deleteThis, 'img/del', "", true, false).then(
            function(){
                var indexToDel = vm.imgFilenamesStyle.indexOf(img);
                vm.imgFilenamesStyle.splice(indexToDel, 1);
                console.log('pageEditorCtrl:: delImg:: DEL request successful: ', vm.imgFilenamesStyle);
            },
            function(){
                console.log('pageEditorCtrl:: delImg:: DEL request failed');
            });
    };

    vm.selectImage = function(img){
        console.log("pageEditorCtrl:: selectedImg: ", img);
        vm["header-img"] = img.Id;
        for(var i = 0; i < vm.imgFilenamesStyle.length; i++){
            vm.imgFilenamesStyle[i].Style = "";
        }
        //console.log("pageEditorCtrl:: updated styles",vm.imgFilenamesStyle);
        img.Style = 'border: 5px solid #ffd72f';
    };
}

function userCtrl($state, gettingFactory, deletingFactory){
    "use strict";

    var vm = this;
    vm.showWarning = false;
    //vm.users = users;
    gettingFactory.getter('users')
        .then(function(response){
            vm.users = response.data;
        }, function(errResonse){
            console.error('Error fetching data.')
        });

    /*
    vm.edit = function(index){
        $state.go("main.userEditor", {userId: index});
    };
*/
    vm.edit = function(index){
        console.log(">>edit user " + index);
        console.log("userCtrl:: edit:: ",vm.users[index].username);
        $state.go("main.userEditor", {username: vm.users[index].username});
    };

    vm.preDelete = function(index){
        vm.data = {
            usernameToDelete: vm.users[index].username
        };
        vm.showWarning = true;
    };

    vm.deleteUser = function(){
        if(vm.delString === "DELETE"){
            vm.delString = "";
            vm.showWarning = false;
            deletingFactory.deleter(vm.data, 'users');
        }
    };
    /*vm.addUser = function(){
        $state.go("main.userEditor", {userId: vm.users.length});
    };*/

    vm.abort = function(){
        vm.showWarning = false;
        vm.data = {};
        vm.delString = "";
    };

    vm.addUser = function(){
        $state.go("main.userEditor", {username: 'NewUser'});
    };



    /*
     vm.deleteUser = function(index){
         this.data = {usernameToDelete: vm.users[index].username};
         deletingFactory.deleter(this.data, 'users');
     };*/
}

function userEditorCtrl($stateParams, gettingFactory, postingFactory){
    "use strict";
    var vm = this;
    vm.permissions = [
        'Administrator', 'Editor', 'SomethingElse'
    ];
    gettingFactory.getter('users')
        .then(function(response){
            vm.users = response.data;
            if ($stateParams.username != "NewUser"){
                for(var i = 0; i < vm.users.length; i++){
                    if (vm.users[i].username == $stateParams.username){
                        vm.user = vm.users[i];
                        break;
                    }
                }
                vm.username = vm.user.username;
                vm.name = vm.user.name;
                vm.email = vm.user.email;
                vm.permission = vm.user.permission;
            }
        }, function(errResonse){
            console.error('Error fetching data.')
        });

    vm.submitChange = function() {
        vm.newData = {
            newUser: {
                username: vm.username,
                name: vm.name,
                email: vm.email,
                permission: vm.permission,
                password: vm.password
            }
        };
        console.log(vm.newData);
        postingFactory.poster(vm.newData, 'users', "main.users");
    };
}