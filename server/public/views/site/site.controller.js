
var app = angular.module("MyCMSApp");

app.config(["$stateProvider", "$urlRouterProvider", SiteConfig]);
app.run(["$rootScope", function($rootScope){
    "use strict";
    $rootScope.normalUrl = 'http://localhost:8000/';
    $rootScope.apiUrl = 'http://localhost:8000/api/';
}]);
app.controller("SiteCtrl", ["gettingFactory",siteCtrl]);
/*app.factory('gettingFactory', ['$http', "$rootScope",function($http, $rootScope){
    var factory = {};
    factory.getter = function(url){
        return $http.get(apiUrl + url);
    };
    factory.test = function(){
        console.log('connected to service');
    };

    return factory;
}]);
*/
function SiteConfig($stateProvider, $urlRouterProvider) {
	"use strict";

	$stateProvider
        .state("site.template",{
            url:":id/:index",
            templateUrl:"views/site/countryTemplate.html",
			controller: ['$stateParams', "gettingFactory", templateCtrl ],
			controllerAs: "template"
        })
    
        .state("site.home", {
            url:"home",
            templateUrl:"views/site/home.html",
        });


	$urlRouterProvider.otherwise("home");
	
}

function templateCtrl($stateParams, gettingFactory){
	"use strict";
	var vm = this;
	console.log($stateParams);
	vm.index = $stateParams.index;

	gettingFactory.getter('pages')
        .then(function(response){
            vm.pages = response.data;
            vm.page = vm.pages[vm.index];
            vm.name = vm.page.name;
			vm.headerImg = vm.page["header-img"];
			vm.caption = vm.page.caption;
			vm.loc = "https://www.google.com/maps/embed/v1/place?key=AIzaSyCDaVCl_oLAE5TsnKVshWXJcGGGR7JA0q0&q="+ vm.name;
			document.getElementById('gmap').src = vm.loc;
            
        }, function(errResonse){
            console.error('Error occurred fetching data.')
    });
}

function siteCtrl(gettingFactory) {
	"use strict";

	var vm = this;
	gettingFactory.getter('pages')
        .then(function(response){
            vm.pages = response.data;
        }, function(errResonse){
            console.error('Error occurred fetching data.')
    });

}